# Keycloak Admin Library

## Basic usage example
```php
$client = (new ClientBuilder())
    ->withServerUrl($server)
    ->withClientId($clientId)
    ->withUsername($keycloakUsername)
    ->withPassword($keycloakPassword)
    ->build();
    
// OR

$client = (new ClientBuilder([
        "realm"=> $kc['realm'],
        "serverUrl"   => $server,
        "clientId"    => $clientId,
        "clientSecret"=> $clientSecret,
        "username"    => $keycloakUsername,
        "password"    => $keycloakPassword,
    ]))->build();
    
// Add a user
$client->realm('master')
    ->users()
    ->create()
    ->username($username)
    ->password($password)
    ->email($email)
    ->save();
```