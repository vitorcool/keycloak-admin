<?php

namespace Scito\Keycloak\Admin\Representations;

use Scito\Keycloak\Admin\Hydrator\Hydrator;

class RoleRepresentationBuilder extends AbstractRepresentationBuilder implements RoleRepresentationBuilderInterface
{
    public function withName(string $name): RoleRepresentationBuilderInterface
    {
        return $this->_setAttribute('name', $name);
    }

    public function withDescription(string $description): RoleRepresentationBuilderInterface
    {
        return $this->_setAttribute('description', $description);
    }

    public function withClientRole(bool $isClientRole): RoleRepresentationBuilderInterface
    {
        return $this->_setAttribute('clientRole', $isClientRole);
    }

    public function withComposite(bool $composite): RoleRepresentationBuilderInterface
    {
        return $this->_setAttribute('composite', $composite);
    }

    public function withContainerId(string $containerId): RoleRepresentationBuilderInterface
    {
        return $this->_setAttribute('containerId', $containerId);
    }

    public function build(): RoleRepresentationInterface
    {
        $data = $this->_getAttributes();
        $hydrator = new Hydrator();
        return $hydrator->hydrate($data, RoleRepresentation::class);
    }
}
