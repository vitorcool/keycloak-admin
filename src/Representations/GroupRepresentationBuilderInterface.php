<?php

namespace Scito\Keycloak\Admin\Representations;

interface GroupRepresentationBuilderInterface
{
    public function withId(string $groupId): GroupRepresentationBuilderInterface;

    public function withName(string $name): GroupRepresentationBuilderInterface;

    public function withPath(string $path): GroupRepresentationBuilderInterface;

    public function withRealmRoles(array $realmRoles): GroupRepresentationBuilderInterface;

    public function withSubGroups(array $subGroups): GroupRepresentationBuilderInterface;

//    public function withAccess(Map $access): GroupRepresentationBuilderInterface;

//    public function withAttributes(Map $attributes): GroupRepresentationBuilderInterface;
//
//    public function withClientRoles(Map $clientRoles): GroupRepresentationBuilderInterface;

    public function build(): GroupRepresentationInterface;
}
