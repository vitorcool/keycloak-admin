<?php

namespace Scito\Keycloak\Admin\Representations;

class UserConsentRepresentation extends AbstractRepresentation implements UserConsentRepresentationInterface
{
    public function __construct(
        ?string $clientId = null,
        ?int $createdDate = null,
        ?array $grantedClientScopes = null,
        ?int $lastUpdatedDate = null
    ) {
        $this->_setAttributes(get_defined_vars());
    }

    public function getClientId(): ?string
    {
        return $this->_getAttribute('clientId');
    }

    public function getCreatedDate(): ?string
    {
        return $this->_getAttribute('createdDate');
    }

    public function getGrantedClientScopes(): ?array
    {
        return $this->_getAttribute('grantedClientScopes');
    }

    public function getLastUpdatedDate(): ?int
    {
        return $this->_getAttribute('lastUpdatedDate');
    }

}
