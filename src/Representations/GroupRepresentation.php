<?php

namespace Scito\Keycloak\Admin\Representations;

class GroupRepresentation extends AbstractRepresentation implements GroupRepresentationInterface
{
    public function __construct(
        ?string $id = null,
        ?string $name = null,
        ?string $path = null,
        ?array $attributes = [],
        ?array $realmRoles = [],
        ?array $clientRoles = [],
        ?array $subGroups = [],
        ?array $access = null
    ) {
        $this->_setAttributes(get_defined_vars());
    }

    public function getId(): ?string
    {
        return $this->_getAttribute('id');
    }

    public function getName(): ?string
    {
        return $this->_getAttribute('name');
    }

    public function getPath(): ?string
    {
        return $this->_getAttribute('path');
    }

    public function getSubGroups()
    {
        return $this->_getAttribute('subGroups');
    }

    public function getRealmRoles()
    {
        return $this->_getAttribute('realmRoles');
    }


    public function getAccess()
    {
        return $this->_getAttribute('access');
    }

    public function getAttributes()
    {
        return $this->_getAttribute('attributes');
    }

    public function getClientRoles()
    {
        return $this->_getAttribute('clientRoles');
    }

}
