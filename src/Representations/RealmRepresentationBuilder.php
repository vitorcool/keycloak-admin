<?php

namespace Scito\Keycloak\Admin\Representations;

use Scito\Keycloak\Admin\Hydrator\Hydrator;

class RealmRepresentationBuilder extends AbstractRepresentationBuilder implements RealmRepresentationBuilderInterface
{
    public function withRealm(string $realm): RealmRepresentationBuilderInterface
    {
        return $this->_setAttribute('realm', $realm);
    }

    public function withEnabled(bool $enabled): RealmRepresentationBuilderInterface
    {
        return $this->_setAttribute('enabled', $enabled);
    }

    public function withDisplayName(string $displayName): RealmRepresentationBuilderInterface
    {
        return $this->_setAttribute('displayName', $displayName);
    }

    public function withAccessCodeLifespan(int $accessCodeLifespan): RealmRepresentationBuilderInterface
    {
        return $this->_setAttribute('accessCodeLifespan', $accessCodeLifespan);
    }

    public function withAccessCodeLifespanLogin(int $accessCodeLifespanLogin): RealmRepresentationBuilderInterface
    {
        return $this->_setAttribute('accessCodeLifespanLogin', $accessCodeLifespanLogin);
    }

    public function withAccessCodeLifespanUserAction(int $accessCodeLifespanUserAction
    ): RealmRepresentationBuilderInterface {
        return $this->_setAttribute('accessCodeLifespanUserAction', $accessCodeLifespanUserAction);
    }

    public function withAccessTokenLifespan(int $accessTokenLifespan): RealmRepresentationBuilderInterface
    {
        return $this->_setAttribute('accessTokenLifespan', $accessTokenLifespan);
    }

    public function withAccessTokenLifespanForImplicitFlow(int $accessTokenLifespanForImplicitFlow
    ): RealmRepresentationBuilderInterface {
        return $this->_setAttribute('accessTokenLifespanForImplicitFlow', $accessTokenLifespanForImplicitFlow);
    }

    public function withAccountTheme(string $accountTheme): RealmRepresentationBuilderInterface
    {
        return $this->_setAttribute('accountTheme', $accountTheme);
    }

    public function withActionTokenGeneratedByAdminLifespan(int $actionTokenGeneratedByAdminLifespan
    ): RealmRepresentationBuilderInterface {
        return $this->_setAttribute('actionTokenGeneratedByAdminLifespan', $actionTokenGeneratedByAdminLifespan);
    }

    public function withActionTokenGeneratedByUserLifespan(int $actionTokenGeneratedByUserLifespan
    ): RealmRepresentationBuilderInterface {
        return $this->_setAttribute('actionTokenGeneratedByUserLifespan', $actionTokenGeneratedByUserLifespan);
    }

    public function withAdminEventsDetailsEnabled(bool $adminEventsDetailsEnabled): RealmRepresentationBuilderInterface
    {
        return $this->_setAttribute('adminEventsDetailsEnabled', $adminEventsDetailsEnabled);
    }

    public function withAdminEventsEnabled(bool $adminEventsEnabled): RealmRepresentationBuilderInterface
    {
        return $this->_setAttribute('adminEventsEnabled', $adminEventsEnabled);
    }

    public function withAdminTheme(string $adminTheme): RealmRepresentationBuilderInterface
    {
        return $this->_setAttribute('adminTheme', $adminTheme);
    }

    public function withAttributes(?array $attributes): RealmRepresentationBuilderInterface
    {
        return $this->_setAttribute('attributes', $attributes);
    }

    public function withBruteForceProtected(bool $bruteForceProtected): RealmRepresentationBuilderInterface
    {
        return $this->_setAttribute('bruteForceProtected', $bruteForceProtected);
    }

    public function withRememberMe(bool $rememberMe): RealmRepresentationBuilderInterface
    {
        return $this->_setAttribute('rememberMe', $rememberMe);
    }

    public function withRoles($roles): RealmRepresentationBuilderInterface
    {
        return $this->_setAttribute('roles', $roles);
    }

    public function build(): RealmRepresentationInterface
    {
        $data = $this->_getAttributes();

        $data['enabled'] = $this->_getAttribute('enabled', false);
        $data['name'] = $this->_getAttribute('realm');

        $hydrator = new Hydrator();

        return $hydrator->hydrate($data, RealmRepresentation::class);
    }
}
