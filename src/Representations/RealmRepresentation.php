<?php

namespace Scito\Keycloak\Admin\Representations;

class RealmRepresentation extends AbstractRepresentation implements RealmRepresentationInterface
{
    public function __construct(
        ?string $id = null,
        ?string $realm = null,
        ?string $displayName = null,
        ?bool $enabled = null
    ) {
        $this->_setAttributes(get_defined_vars());
    }

    public function getId(): ?string
    {
        return $this->_getAttribute('id');
    }

    public function getRealm(): string
    {
        return $this->_getAttribute('realm');
    }

    public function getEnabled(): bool
    {
        return $this->_getAttribute('enabled', false);
    }

    public function getDisplayName(): string
    {
        return $this->_getAttribute('displayName');
    }
}
