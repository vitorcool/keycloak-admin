<?php

namespace Scito\Keycloak\Admin\Representations;

use DateTime;
use function get_defined_vars;
use Scito\Keycloak\Admin\Representations\Attribute;
use stdClass;

class UserRepresentation extends AbstractRepresentation implements UserRepresentationInterface
{
    /**
     * User constructor.
     *
     * @param string $id
     * @param string[] $requiredActions
     * @param Attribute $attributes
     * @param string $username
     * @param string $email
     * @param string $firstName
     * @param string $lastName
     * @param CredentialRepresentation[] $credentials
     * @param bool $enabled
     * @param bool $emailVerified
     * @param DateTime $created
     */
    public function __construct(
        string $id = null,
        array $requiredActions = [],
        array $attributes = null,
        string $username = null,
        string $email = null,
        string $firstName = null,
        string $lastName = null,
        array $credentials = [],
        bool $enabled = false,
        bool $emailVerified = false,
        DateTime $created = null
    ) {
        $this->_setAttributes(get_defined_vars());
    }

    public function getId(): ?string
    {
        return $this->_getAttribute('id');
    }

    public function getEmail(): ?string
    {
        return $this->_getAttribute('email');
    }

    public function getUsername(): ?string
    {
        return $this->_getAttribute('username');
    }

    public function getFirstName(): ?string
    {
        return $this->_getAttribute('firstName');
    }

    public function getLastName(): ?string
    {
        return $this->_getAttribute('lastName');
    }

    public function getEnabled(): ?bool
    {
        return $this->_getAttribute('enabled', false);
    }

    public function getCreated()
    {
        return $this->_getAttribute('created');
    }

    public function getCredentials(): array
    {
        return $this->_getAttribute('credentials', []);
    }

    public function getRequiredActions(): array
    {
        return $this->_getAttribute('requiredActions', []);
    }

    public function getAttributes(): array
    {
        return $this->_getAttribute('attributes', []);
    }
}
