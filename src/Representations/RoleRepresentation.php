<?php

namespace Scito\Keycloak\Admin\Representations;

class RoleRepresentation extends AbstractRepresentation implements RoleRepresentationInterface
{
    public function __construct(
        ?string $id = null,
        ?string $name = null,
        ?string $containerId = null,
        ?string $description = null,
        ?bool $composite = null,
        ?bool $clientRole = null,
        ?array $attributes = null
    ) {
        $this->_setAttributes(get_defined_vars());
    }

    public function getId(): ?string
    {
        return $this->_getAttribute('id');
    }

    public function getName(): ?string
    {
        return $this->_getAttribute('name');
    }

    public function getDescription(): ?string
    {
        return $this->_getAttribute('description');
    }

    public function getContainerId(): ?string
    {
        return $this->_getAttribute('containerId');
    }

    public function isClientRole(): ?bool
    {
        return $this->_getAttribute('clientRole');
    }

    public function isComposite(): ?bool
    {
        return $this->_getAttribute('composite');
    }
}
