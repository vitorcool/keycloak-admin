<?php

namespace Scito\Keycloak\Admin\Representations;

class CredentialRepresentation extends AbstractRepresentation
{
    /**
     * CredentialRepresentation constructor.
     * @param string $type
     * @param string $value
     * @param bool $temporary
     */
    public function __construct(string $type = null, string $value = null, bool $temporary = false)
    {
        $this->_setAttributes(get_defined_vars());
    }

    public function getType(): string
    {
        return $this->_getAttribute('type');
    }

    public function getValue(): string
    {
        return $this->_getAttribute('value');
    }

    public function getTemporary(): ?bool
    {
        return $this->_getAttribute('temporary');
    }
}
