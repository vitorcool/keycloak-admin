<?php

namespace Scito\Keycloak\Admin\Representations;

use ArrayObject;
use function array_filter;
use function array_key_exists;
use function call_user_func;
use function is_callable;
use function iterator_to_array;
use ReflectionClass;
use ReflectionMethod;

class RepresentationCollection implements RepresentationCollectionInterface
{
    private $items;

    public function __construct(array $items = [])
    {
        $this->items = $items;
    }

    public function count()
    {
        return count($this->items);
    }

    public function filter(callable $filter): RepresentationCollectionInterface
    {
        return new RepresentationCollection(array_filter(iterator_to_array($this), $filter));
    }

    public function map(callable $callback): RepresentationCollectionInterface
    {
        return new RepresentationCollection(array_map($callback, iterator_to_array($this)));
    }

    private function dismount($object) {
        $objClass = get_class($object);
        $reflectionClass = new ReflectionClass($objClass);
        $array = [];
        foreach ($reflectionClass->getProperties() as $property) {
            $property->setAccessible(true);
            $array[$property->getName()] = $property->getValue($object);
            $property->setAccessible(false);
        }
        return $array;
    }

    private function isCallable($object, $method) {
        if (method_exists($object, $method))
        {
            $reflection = new ReflectionMethod($object, $method);
            return $reflection->isPublic();
        }

        return false;
    }

    private function any_to_array($any) {
        // convert obj to array
        if(is_object($any)) {
            if($this->isCallable($any, 'toArray')){
                $any = $any->toArray();
            }else{
                $any = $this->dismount($any);
            }
        }
        // when array itherate recursive
        if(is_array($any)) {
            $new = array();
            foreach($any as $key => $val) {
                $new[$key] = $this->any_to_array($val);
            }
        }else
            $new = $any;
        return $new;
    }

    public function toArray(): array
    {
        $ret = $this->any_to_array($this->items);
        return $ret;
    }

    public function offsetExists($offset)
    {
        return array_key_exists($offset, $this->items);
    }

    public function offsetGet($offset)
    {
        return $this->items[$offset];
    }

    public function offsetSet($offset, $value)
    {
        $this->items[$offset] = $value;
    }

    public function offsetUnset($offset)
    {
        unset($this->items[$offset]);
    }

    public function first(?callable $callback = null): ?RepresentationInterface
    {
        if (!is_callable($callback)) {
            return count($this->items) > 0 ? reset($this->items) : null;
        }
        foreach ($this->getIterator() as $item) {
            if (call_user_func($callback, $item)) {
                return $item;
            }
        }
        return null;
    }

    public function getIterator(): iterable
    {
        return new ArrayObject($this->items);
    }
}
