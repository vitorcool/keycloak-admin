<?php

namespace Scito\Keycloak\Admin\Representations;

interface RepresentationInterface
{
    public function toJson(): string;

    public function toArray(): array;
}
