<?php

namespace Scito\Keycloak\Admin\Representations;

interface GroupRepresentationInterface extends RepresentationInterface
{


    /**
     * @return string
     */
    public function getId(): ?string;

    /**
     * @return string
     */
    public function getName(): ?string;

    /**
     * @return string
     */
    public function getPath(): ?string;

    public function getSubGroups();


    public function getRealmRoles();

    public function getAccess();

    public function getAttributes();

    public function getClientRoles();
}
