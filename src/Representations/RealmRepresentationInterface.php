<?php

namespace Scito\Keycloak\Admin\Representations;

interface RealmRepresentationInterface
{

    public function getId(): ?string;

    public function getRealm(): string;

    public function getEnabled(): bool;

    public function getDisplayName(): string;
}
