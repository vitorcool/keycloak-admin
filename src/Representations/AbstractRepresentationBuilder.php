<?php

namespace Scito\Keycloak\Admin\Representations;

abstract class AbstractRepresentationBuilder
{
    protected $attributes = [];

    protected function _getAttribute($key, $default = null)
    {
        return array_key_exists($key, $this->attributes) ? $this->attributes[$key] : $default;
    }

    protected function _getAttributes()
    {
        return $this->attributes;
    }

    protected function _setAttributes(array $attributes)
    {
        foreach ($attributes as $k => $v) {
            $this->_setAttribute($k, $v);
        }
        return $this;
    }

    protected function _setAttribute($name, $value)
    {
        $this->attributes[$name] = $value;
        return $this;
    }
}
