<?php

namespace Scito\Keycloak\Admin\Representations;

use Scito\Keycloak\Admin\Hydrator\Hydrator;

class GroupRepresentationBuilder extends AbstractRepresentationBuilder implements GroupRepresentationBuilderInterface
{
    public function withId(string $groupId): GroupRepresentationBuilderInterface
    {
        return $this->_setAttribute('groupId', $groupId);
    }

    public function withName(string $name): GroupRepresentationBuilderInterface
    {
        return $this->_setAttribute('name', $name);
    }

    public function withPath(string $path): GroupRepresentationBuilderInterface{
        return $this->_setAttribute('$path', $path);
    }

    public function withRealmRoles(array $realmRoles): GroupRepresentationBuilderInterface{
        return $this->_setAttribute('realmRoles', $realmRoles);
    }

    public function withSubGroups(array $subGroups): GroupRepresentationBuilderInterface{
        return $this->_setAttribute('subGroups', $subGroups);
    }

//    public function withAccess(Map $access): GroupRepresentationBuilderInterface{
//        return $this->setAttribute('access', $access);
//    }
//
//    public function withAttributes(Map $attributes): GroupRepresentationBuilderInterface{
//        return $this->setAttribute('attributes', $attributes);
//    }
//
//    public function withClientRoles(Map $clientRoles): GroupRepresentationBuilderInterface{
//        return $this->setAttribute('clientRoles', $clientRoles);
//    }

    public function build(): GroupRepresentationInterface
    {
        $data = $this->_getAttributes();
        $hydrator = new Hydrator();
        return $hydrator->hydrate($data, GroupRepresentation::class);
    }
}
