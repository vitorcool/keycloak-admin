<?php

namespace Scito\Keycloak\Admin\Exceptions;

use RuntimeException;

class CannotRetrieveRealmsRepresentationException extends RuntimeException
{
}
