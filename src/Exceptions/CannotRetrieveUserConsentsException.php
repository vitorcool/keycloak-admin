<?php

namespace Scito\Keycloak\Admin\Exceptions;

use RuntimeException;

class CannotRetrieveUserConsentsException extends RuntimeException
{

}
