<?php

namespace Scito\Keycloak\Admin\Exceptions;

use RuntimeException;

class CannotRetrieveGroupByNameException extends RuntimeException
{

}
