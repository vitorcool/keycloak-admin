<?php

namespace Scito\Keycloak\Admin\Exceptions;

use RuntimeException;

class CannotRetrieveGroupsException extends RuntimeException
{

}
