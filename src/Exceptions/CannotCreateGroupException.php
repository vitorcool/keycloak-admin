<?php

namespace Scito\Keycloak\Admin\Exceptions;

use RuntimeException;

class CannotCreateGroupException extends RuntimeException
{

}
