<?php

namespace Scito\Keycloak\Admin\Exceptions;

use RuntimeException;

class CannotDeleteGroupException extends RuntimeException
{

}
