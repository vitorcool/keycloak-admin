<?php

namespace Scito\Keycloak\Admin\Exceptions;

use RuntimeException;

class CannotAddUserGroupException extends RuntimeException
{

}
