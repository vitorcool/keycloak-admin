<?php

namespace Scito\Keycloak\Admin\Exceptions;

use RuntimeException;

class CannotRetrieveUserGroupByNameException extends RuntimeException
{
}
