<?php

namespace Scito\Keycloak\Admin\Exceptions;

use RuntimeException;

class CannotDeleteUserGroupException extends RuntimeException
{

}
