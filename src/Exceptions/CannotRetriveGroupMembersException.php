<?php

namespace Scito\Keycloak\Admin\Exceptions;

use RuntimeException;

class CannotRetriveGroupMembersException extends RuntimeException
{

}
