<?php

namespace Scito\Keycloak\Admin\Exceptions;

use RuntimeException;

class CannotRetrieveGroupRepresentationException extends RuntimeException
{
}
