<?php

namespace Scito\Keycloak\Admin\Token;

use DateInterval;
use GuzzleHttp\Client;
use RuntimeException;
use function date_create;
use function json_decode;

class TokenManager
{
    /**
     * @var Token[]
     */
    private $tokens;

    private $username;

    private $password;

    private $client;

    private $clientId;

    private $clientSecret;

    public function __construct( Client $client, array $args=[] ) //string $username,string $password,string $clientId,string $clientSecret)
    {
        $this->client = $client;
        foreach($args as $attrName => $attrValue){
            if(is_null($this->$attrName)) {
                $this->$attrName = $attrValue;
            }
        }
//        $this->username = $args["username"];
//        $this->password = $args["password"];
//        $this->clientId = $args["clientId"];
//        $this->clientSecret = $args["clientSecret"];
//        $this->client = $args["client"];
//
//        if(isset($args["token"])){
//            $expires = date_create()->add(new DateInterval(sprintf('PT%dS', $args['token_expires_in']?? time() )));
//            $this->tokens[] = new Token("Bearer", $args["token"], $expires);
//        }
    }

    public function addToken($realm,$token_type,$access_token,$expires){
        // fix $expires
        if(is_int($expires)) {
            $expires=date_create()->add(new DateInterval(sprintf('PT%dS', $expires)));
        }
        $this->tokens[$realm] = new Token($token_type, $access_token, $expires);
        return $this;
    }
    /**
     * @param $realm
     * @return Token
     */
    public function getToken($realm,$grant_type="password")
    {
        if ($this->tokens[$realm] && $this->tokens[$realm]->isValid()) {
            return $this->tokens[$realm];
        }else{
            $z=0;
        }

        $response = $this->client->post("/auth/realms/{$realm}/protocol/openid-connect/token",
            ['form_params' => [
                'username' => $this->username,
                'password' => $this->password,
                'client_id' => $this->clientId,
                'client_secret' => $this->clientSecret,
                'grant_type' => $grant_type,
                ]]);

        if (200 !== $response->getStatusCode()) {
            throw new RuntimeException("Error getting token");
        }

        $data = json_decode((string)$response->getBody(), true);

        //$expires = date_create()->add(new DateInterval(sprintf('PT%dS', $data['expires_in'])));

        $this->addToken($realm,$data['token_type'], $data['access_token'], $data['expires_in']);

        return $this->tokens[$realm];
    }
}
