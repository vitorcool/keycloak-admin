<?php

namespace Scito\Keycloak\Admin\Resources;

use GuzzleHttp\ClientInterface;
use Scito\Keycloak\Admin\Exceptions\CannotCreateRealmException;
use Scito\Keycloak\Admin\Exceptions\CannotRetrieveRealmsRepresentationException;
use Scito\Keycloak\Admin\Hydrator\HydratorInterface;
use Scito\Keycloak\Admin\Representations\RealmRepresentation;
use Scito\Keycloak\Admin\Representations\RealmRepresentationInterface;
use Scito\Keycloak\Admin\Representations\RepresentationCollection;
use Scito\Keycloak\Admin\Representations\RepresentationCollectionInterface;

class RealmsResource implements RealmsResourceInterface
{
    /**
     * @var ClientInterface
     */
    private $client;
    /**
     * @var ResourceFactoryInterface
     */
    private $resourceFactory;
    /**
     * @var HydratorInterface
     */
    private $hydrator;

    public function __construct(
        ClientInterface $client,
        ResourceFactoryInterface $resourceFactory,
        HydratorInterface $hydrator
    ) {
        $this->client = $client;
        $this->resourceFactory = $resourceFactory;
        $this->hydrator = $hydrator;
    }

    public function create(?array $options = null): RealmCreateResourceInterface
    {
        $createResource = $this->resourceFactory->createRealmCreateResource();
        if (null !== $options) {
            foreach ($options as $key => $value) {
                $createResource->$key($value);
            }
        }
        return $createResource;
    }

    public function realm($realm): RealmResourceInterface
    {
        return $this->resourceFactory->createRealmResource($realm);
    }

    public function add(RealmRepresentationInterface $realm): void
    {
        $data = $this->hydrator->extract($realm);

        $data['realm'] = $data['name'];
        unset($data['name'], $data['id'], $data['created']);

        $response = $this->client->post('/auth/admin/realms', ['body' => json_encode($data)]);

        if (201 !== $response->getStatusCode()) {
            throw new CannotCreateRealmException("Cannot create realm $data[realm]");
        }
    }

    public function all(): array
    {
        $response = $this->client->get("/auth/admin/realms");

        if (200 !== $response->getStatusCode()) {
            throw new CannotRetrieveRealmsRepresentationException();
        }

        $json = (string)$response->getBody();

        $realms = json_decode($json, true);

        return $realms;

//        $items = array_map(function ($realm) {
//            $ret= $this->hydrator->hydrate(['name'=>$realm['realm']], RealmRepresentation::class);
//            return $ret;
//        }, $realms);
//
//        return new RepresentationCollection($items);
    }
}
