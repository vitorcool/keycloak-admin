<?php

namespace Scito\Keycloak\Admin\Resources;

use Scito\Keycloak\Admin\Representations\RepresentationCollectionInterface;

interface UserConsentsResourceInterface
{
    public function all(): RepresentationCollectionInterface;

}
