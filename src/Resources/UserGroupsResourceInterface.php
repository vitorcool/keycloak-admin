<?php

namespace Scito\Keycloak\Admin\Resources;


use Scito\Keycloak\Admin\Representations\GroupRepresentationInterface;
use Scito\Keycloak\Admin\Representations\GroupsRepresentationInterface;
use Scito\Keycloak\Admin\Representations\RepresentationCollectionInterface;

interface UserGroupsResourceInterface
{
    /**
     * @param string $id
     * @return GroupResourceInterface
     */
    public function get(string $id): GroupResourceInterface;//UserGroupResourceInterface;
    public function getByName(string $name): UserGroupResourceInterface;

    /**
     * @return RepresentationCollectionInterface
     */
    public function All(): RepresentationCollectionInterface;

    public function count(): int;

    public function add(GroupRepresentationInterface $group): bool;

}
