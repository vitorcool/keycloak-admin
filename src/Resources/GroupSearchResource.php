<?php

namespace Scito\Keycloak\Admin\Resources;

use GuzzleHttp\ClientInterface;
use RuntimeException;
use Scito\Keycloak\Admin\Hydrator\HydratorInterface;
use Scito\Keycloak\Admin\Representations\GroupRepresentation;
use Scito\Keycloak\Admin\Representations\RepresentationCollection;
use function http_build_query;
use function json_decode;

class GroupSearchResource implements GroupSearchResourceInterface
{
    use SearchableResource;

    /**
     * @var ClientInterface
     */
    private $client;
    /**
     * @var ResourceFactoryInterface
     */
    private $resourceFactory;
    /**
     * @var string
     */
    private $realm;
    /**
     * @var HydratorInterface
     */
    private $hydrator;

    private $userId=null;

    public function __construct(
        ClientInterface $client,
        ResourceFactoryInterface $resourceFactory,
        HydratorInterface $hydrator,
        string $realm,
        ?string $userId=null
    ) {
        $this->client = $client;
        $this->resourceFactory = $resourceFactory;
        $this->realm = $realm;
        $this->hydrator = $hydrator;
        $this->userId = $userId;
    }

    private function resUrl()
    {
        return  is_null($this->userId)
            ? "/auth/admin/realms/{$this->realm}/groups"
            : "/auth/admin/realms/{$this->realm}/users/{$this->userId}/groups";
    }

    public function offset(int $offset = null): GroupSearchResourceInterface
    {
        $this->withSearchOption('first', $offset);
        return $this;
    }

    public function limit(int $limit = null): GroupSearchResourceInterface
    {
        $this->withSearchOption('max', $limit);
        return $this;
    }

    public function id(string $id): GroupSearchResourceInterface
    {
        $this->withSearchOption('id', $id);
        return $this;
    }

    public function name(string $name): GroupSearchResourceInterface
    {
        $this->withSearchOption('name', $name);
        return $this;
    }

    public function path(string $path): GroupSearchResourceInterface
    {
        $this->withSearchOption('path', $path);
        return $this;
    }

    public function query(string $query): GroupSearchResourceInterface
    {
        $this->withSearchOption('search', $query);
        return $this;
    }

    public function __call($name, $arguments)
    {
        throw new RuntimeException("Unknown searchable method [$name]");
    }

    public function getIterator()
    {
        return $this->get();
    }

    public function get()
    {
        $options = $this->getSearchOptions();
        $queryString = '';
        if (!empty($options)) {
            $queryString = '?' . http_build_query($options);
        }

        $response = $this->client->get($this->resUrl().$queryString);
        if (200 !== $response->getStatusCode()) {
            throw new CannotRetrieveGroupsException("Unable to retrieve groups");
        }

        $json = (string)$response->getBody();
        $groups = json_decode($json, true);

        $items = array_map(function ($group) {
            return $this->hydrator->hydrate($group, GroupRepresentation::class);
        }, $groups);

        return new RepresentationCollection($items);
    }

    public function first()
    {
        $result = $this->get();
        if (count($result) > 0) {
            return $result[0];
        }
        return null;
    }
}
