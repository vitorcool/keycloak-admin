<?php

namespace Scito\Keycloak\Admin\Resources;

use Scito\Keycloak\Admin\Representations\GroupRepresentationInterface;
interface GroupCreateResourceInterface
{
    public function groupId(string $groupId): GroupCreateResourceInterface;

    public function name(string $name): GroupCreateResourceInterface;

    public function path(string $path): GroupCreateResourceInterface;

    /**
     * @param string[] $realmRoles
     * @return GroupCreateResourceInterface
     */
    public function realmRoles(array $realmRoles): GroupCreateResourceInterface;

    /**
     * @param GroupRepresentationInterface $subGroups
     * @return GroupCreateResourceInterface
     */
    public function subGroups(array $subGroups): GroupCreateResourceInterface;


    public function save(): GroupResourceInterface;
}
