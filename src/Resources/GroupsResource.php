<?php

namespace Scito\Keycloak\Admin\Resources;

use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use Scito\Keycloak\Admin\Exceptions\CannotCreateGroupException;
use Scito\Keycloak\Admin\Exceptions\CannotRetrieveGroupByNameException;
use Scito\Keycloak\Admin\Hydrator\HydratorInterface;
use Scito\Keycloak\Admin\Representations\GroupRepresentationInterface;
use Scito\Keycloak\Admin\Representations\RepresentationCollectionInterface;
use Scito\Keycloak\Admin\Representations\RepresentationCollection;
class GroupsResource implements GroupsResourceInterface
{
    /**
     * @var Client
     */
    private $client;
    /**
     * @var string
     */
    private $realm;

    private $resourceFactory;

    private $hydrator;

    public function __construct(
        ClientInterface $client,
        ResourceFactoryInterface $resourceFactory,
        HydratorInterface $hydrator,
        string $realm
    ) {
        $this->client = $client;
        $this->realm = $realm;
        $this->hydrator = $hydrator;
        $this->resourceFactory = $resourceFactory;
    }

    private function resUrl()
    {
        return "/auth/admin/realms/{$this->realm}/groups";
    }

    public function count(): int
    {
        $response = $this->client->post($this->resUrl()."/count");
        return $response;
    }

    public function update(GroupRepresentationInterface $group): void
    {
        $id = $group->getId();

        if (null == $id) {
            throw new CannotUpdateGroupException("Group id missing");
        }

        $data = $this->hydrator->extract($group);
        unset($data['created'], $data['groupname']);

        $response = $this->client->put($this->resUrl()."/{$id}", ['body' => json_encode($data)]);

        if (204 !== $response->getStatusCode()) {
            throw new CannotUpdateGroupException("Group [$id] cannot be updated");
        }
    }

    public function add(GroupRepresentationInterface $group): GroupResourceInterface
    {
        $data = $this->hydrator->extract($group);
        unset($data['id'], $data['created']);

        $response = $this->client->post($this->resUrl(), ['body' => json_encode($data)]);

        if (201 !== $response->getStatusCode()) {
            $body=(string)$response->getBody();
            throw new CannotCreateGroupException("Unable to create group {$body}");
        }

        $location = $response->getHeaderLine('Location');
        $parts = array_filter(explode('/', $location), 'strlen');
        $id = end($parts);
        return $this->get($id);
    }

    public function get(string $id): GroupResourceInterface
    {
        return $this->resourceFactory->createGroupResource($this->realm, $id);
    }

    private $idOfName=[];  // need load before use
    public function getByName(string $name): GroupResourceInterface
    {
        // load idOfName Array Before
        $this->idOfName  = empty($this->idOfName)
            ? $this->all()->toArray()
            : $this->idOfName;

        if(is_numeric(key($this->idOfName)))
            $this->idOfName=array_combine(array_column($this->idOfName,'name'),$this->idOfName);

        $id=$this->idOfName[$name]['id'] ?? null;
        if(is_null($id)){
            throw new CannotRetrieveGroupByNameException("Unable to retrieve group with name {$name}");
        }
        return $this->get($id);
    }

    /**
     * @return RepresentationCollection
     */
    public function All(): RepresentationCollectionInterface
    {
        $ret = $this->search([]);
        $ret =  $ret->get();
        return $ret;
    }

    public function search(?array $options = null): GroupSearchResourceInterface
    {
        $searchResource = $this->resourceFactory->createGroupSearchResource($this->realm);

        if (null !== $options) {
            foreach ($options as $k => $v) {
                $searchResource->$k($v);
            }
        }

        return $searchResource;
    }

    /**
     * @param array|null $options
     * @return UserCreateResourceInterface
     */
    public function create(?array $options = null): GroupCreateResourceInterface
    {
        $builderResource = $this->resourceFactory->createGroupCreateResource($this->realm);
        if (null !== $options) {
            foreach ($options as $key => $value) {
                $builderResource->$key($value);
            }
        }
        return $builderResource;
    }

    public function deleteById($id)
    {
        $response = $this->client->delete($this->resUrl()."/{$id}");

        if (204 != $response->getStatusCode()) {
            throw new CannotDeleteGroupException("Group with id [$id] cannot be deleted");
        }
    }

}
