<?php

namespace Scito\Keycloak\Admin\Resources;

use GuzzleHttp\ClientInterface;
use Scito\Keycloak\Admin\Hydrator\HydratorInterface;
use Scito\Keycloak\Admin\Representations\RepresentationCollection;
use Scito\Keycloak\Admin\Representations\RepresentationCollectionInterface;
use Scito\Keycloak\Admin\Representations\RoleRepresentation;

class UserConsentsResource implements UserConsentsResourceInterface
{
    /**
     * @var ResourceFactoryInterface
     */
    private $resourceFactory;
    /**
     * @var ClientInterface
     */
    private $client;
    /**
     * @var string
     */
    private $realm;
    /**
     * @var string
     */
    private $id;
    /**
     * @var HydratorInterface
     */
    private $hydrator;

    public function __construct(
        ResourceFactoryInterface $resourceFactory,
        HydratorInterface $hydrator,
        ClientInterface $client,
        string $realm,
        string $id
    ) {
        $this->resourceFactory = $resourceFactory;
        $this->client = $client;
        $this->realm = $realm;
        $this->id = $id;
        $this->hydrator = $hydrator;
    }

    public function all(): RepresentationCollectionInterface
    {
        $response = $this->client->get("/auth/admin/realms/{$this->realm}/users/{$this->id}/consents");

        if (200 !== $response->getStatusCode()) {
            throw new CannotRetrieveUserConsentsException();
        }

        $json = (string)$response->getBody();
        $data = json_decode($json, true);

        $items = [];
        foreach (['client', 'realm'] as $scope) {
            $key = $scope . 'Mappings';
            if (!array_key_exists($key, $data)) {
                continue;
            }
            foreach ($data[$key] as $role) {
                $items[] = $this->hydrator->hydrate($role, UserConsentRepresentation::class);
            }
        }
        return new RepresentationCollection($items);
    }


}
