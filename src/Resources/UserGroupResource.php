<?php

namespace Scito\Keycloak\Admin\Resources;

use GuzzleHttp\ClientInterface;
use Scito\Keycloak\Admin\Exceptions\CannotDeleteGroupException;
use Scito\Keycloak\Admin\Exceptions\CannotDeleteRoleException;
use Scito\Keycloak\Admin\Exceptions\CannotDeleteUserGroupException;
use Scito\Keycloak\Admin\Exceptions\CannotRetrieveRoleRepresentationException;
use Scito\Keycloak\Admin\Hydrator\HydratorInterface;
use Scito\Keycloak\Admin\Representations\GroupRepresentation;
use Scito\Keycloak\Admin\Representations\GroupRepresentationInterface;
use Scito\Keycloak\Admin\Representations\RoleRepresentation;
use Scito\Keycloak\Admin\Representations\RoleRepresentationInterface;
use function json_decode;

class UserGroupResource implements UserGroupResourceInterface
{
    /**
     * @var ClientInterface
     */
    private $client;
    /**
     * @var ResourceFactoryInterface
     */
    private $resourceFactory;
    /**
     * @var string
     */
    private $realm;
    /**
     * @var string
     */
    private $name;
    /**
     * @var HydratorInterface
     */
    private $hydrator;


    public function __construct(
        ClientInterface $client,
        ResourceFactoryInterface $resourceFactory,
        HydratorInterface $hydrator,
        string $realm,
        string $name
    ) {
        $this->client = $client;
        $this->resourceFactory = $resourceFactory;
        $this->realm = $realm;
        $this->name = $name;
        $this->hydrator = $hydrator;
    }

    private function resUrl()
    {
        return "/auth/admin/realms/{$this->realm}/users/{$this->id}/groups/{$this->name}";
    }

    public function toRepresentation(): GroupRepresentationInterface
    {
        $response = $this->client->get( $this->resUrl() );

        if (200 !== $response->getStatusCode()) {
            throw new CannotRetrieveGroupRepresentationException("Unable to retrieve [$this->name] group details");
        }

        $json = (string)$response->getBody();
        $data = json_decode($json, true);

        return $this->hydrator->hydrate($data, GroupRepresentation::class);
    }


//    public function access(): AccessResourceInterface;

//    public function attributes(): AttributesResourceInterface;

//    public function clientRoles(): ClientRolesResourceInterface;


    public function getId(): string
    {
        return $this->toRepresentation()->getId();
    }

    public function getName(): string
    {
        return $this->realm;
    }

    public function getPath(): string
    {
        return $this->toRepresentation()->getPath();
    }

    /**
     * @return string[]
     */
    public function getRealmRoles(): iterable
    {
        return $this->toRepresentation()->getRealmRoles();
    }

    /**
     * @return UserGroupsRepresentationInterface
     */
    public function getSubGroups(): GroupsRepresentationInterface
    {
        return $this->toRepresentation()->getSubGroups();
    }


    public function getRealm(): string
    {
        return $this->realm;
    }


    public function delete(): void
    {
        $response = $this->client->delete( $this->resUrl() );

        if (204 !== $response->getStatusCode()) {
            $body=(string)$response->getBody();
            $body=empty($body) ? '('.$response->getReasonPhrase().')' : $body;
            throw new CannotDeleteUserGroupException("Group {$this->name} of realm {$this->realm} cannot be deleted {$body}");
        }

    }

}
