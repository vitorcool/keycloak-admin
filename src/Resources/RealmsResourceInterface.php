<?php

namespace Scito\Keycloak\Admin\Resources;

use Scito\Keycloak\Admin\Representations\RealmRepresentationInterface;
use Scito\Keycloak\Admin\Representations\RepresentationCollectionInterface;

interface RealmsResourceInterface
{
    /**
     * @param $realm
     * @return RealmResourceInterface
     */
    public function realm($realm): RealmResourceInterface;

    /**
     * @param RealmRepresentationInterface $realm
     */
    public function add(RealmRepresentationInterface $realm): void;

    /**
     * @param array|null $options
     * @return RealmCreateResourceInterface
     */
    public function create(?array $options = null): RealmCreateResourceInterface;

    /**
     * @return array
     */
    public function all(): array;
}
