<?php

namespace Scito\Keycloak\Admin\Resources;

use GuzzleHttp\ClientInterface;
use Scito\Keycloak\Admin\Exceptions\CannotDeleteGroupException;
use Scito\Keycloak\Admin\Hydrator\HydratorInterface;
use Scito\Keycloak\Admin\Representations\GroupRepresentation;
use Scito\Keycloak\Admin\Representations\GroupRepresentationInterface;
use Scito\Keycloak\Admin\Representations\RepresentationCollection;
use Scito\Keycloak\Admin\Representations\RepresentationCollectionInterface;
use function json_decode;
use Scito\Keycloak\Admin\Representations\UserRepresentation;

class GroupResource implements GroupResourceInterface
{
    /**
     * @var ClientInterface
     */
    private $client;
    /**
     * @var ResourceFactoryInterface
     */
    private $resourceFactory;
    /**
     * @var string
     */
    private $realm;
    /**
     * @var string
     */
    private $id;
    /**
     * @var HydratorInterface
     */
    private $hydrator;

    public function __construct(
        ClientInterface $client,
        ResourceFactoryInterface $resourceFactory,
        HydratorInterface $hydrator,
        string $realm,
        string $id
    ) {
        $this->client = $client;
        $this->resourceFactory = $resourceFactory;
        $this->realm = $realm;
        $this->id = $id;
        $this->hydrator = $hydrator;
    }

    private function resUrl()
    {
        return "/auth/admin/realms/{$this->realm}/groups/{$this->id}";
    }

    public function toRepresentation(): GroupRepresentationInterface
    {
        $response = $this->client->get( $this->resUrl() );

        if (200 !== $response->getStatusCode()) {
            throw new CannotRetrieveGroupRepresentationException("Unable to retrieve [$this->id] group details");
        }

        $json = (string)$response->getBody();
        $data = json_decode($json, true);

        return $this->hydrator->hydrate($data, GroupRepresentation::class);
    }


//    public function access(): AccessResourceInterface;

//    public function attributes(): AttributesResourceInterface;

//    public function clientRoles(): ClientRolesResourceInterface;


    public function getId(): string
    {
        return $this->toRepresentation()->getId();
    }

    public function getName(): string
    {
        return $this->realm;
    }

    public function getPath(): string
    {
        return $this->toRepresentation()->getPath();
    }

    /**
     * @return string[]
     */
    public function getRealmRoles(): iterable
    {
        return $this->toRepresentation()->getRealmRoles();
    }

    /**
     * @return GroupsRepresentationInterface
     */
    public function getSubGroups(): GroupsRepresentationInterface
    {
        return $this->toRepresentation()->getSubGroups();
    }


    public function getRealm(): string
    {
        return $this->realm;
    }


    public function delete(): void
    {
        $response = $this->client->delete( $this->resUrl() );

        if (204 !== $response->getStatusCode()) {
            $body=(string)$response->getBody();
            $body=empty($body) ? '('.$response->getReasonPhrase().')' : $body;
            throw new CannotDeleteGroupException("Group {$this->name} of realm {$this->realm} cannot be deleted {$body}");
        }

    }

    public function update(?array $options = []): GroupUpdateResourceInterface
    {
        $updateResource = $this->resourceFactory->createGroupUpdateResource($this->realm, $this->name);
        if (null !== $options) {
            foreach ($options as $key => $value) {
                $updateResource->$key($value);
            }
        }
        return $updateResource;
    }

    public function getMembers(?array $options = []):  RepresentationCollectionInterface
    {
        $url = $this->resUrl().'/members';
        $options =array_intersect_key(['first','max'], $options);
        $query =http_build_query($options);
        $url .= empty($query)?'':'?'.$query;

        $response = $this->client->get($url);

        if (200 !== $response->getStatusCode()) {
            $body=(string)$response->getBody();
            $body=empty($body) ? '('.$response->getReasonPhrase().')' : $body;
            throw new CannotRetriveGroupMembersException("Cannot retrieve members of Group {$this->name}, realm {$this->realm}. {$body}");
        }

        $json = (string)$response->getBody();
        $data = json_decode($json, true);

        $items = [];
        foreach ($data as $user) {
            $items[] = $this->hydrator->hydrate($user, UserRepresentation::class);
        }

        return new RepresentationCollection($items);
    }
}
