<?php

namespace Scito\Keycloak\Admin\Resources;

use Scito\Keycloak\Admin\Representations\GroupRepresentationBuilderInterface;
use Scito\Keycloak\Admin\Representations\GroupRepresentationInterface;

class GroupCreateResource implements GroupCreateResourceInterface
{


    /**
     * @var GroupsResourceInterface
     */
    private $groupsResource;
    /**
     * @var GroupRepresentationBuilderInterface
     */
    private $builder;
    /**
     * @var string
     */
    private $realm;

    public function __construct(
        GroupsResourceInterface $groupsResource,
        GroupRepresentationBuilderInterface $builder,
        string $realm
    ) {
        $this->groupsResource = $groupsResource;
        $this->builder = $builder;
        $this->realm = $realm;
    }

    public function groupId(string $groupId): GroupCreateResourceInterface
    {
        $this->builder->withGroupId($groupId);
        return $this;
    }

    public function name(string $name): GroupCreateResourceInterface
    {
        $this->builder->withName($name);
        return $this;
    }

    public function path(string $path): GroupCreateResourceInterface
    {
        $this->builder->withPath($path);
        return $this;
    }


    public function realmRoles(array $realmRoles): GroupCreateResourceInterface
    {
        $this->builder->withRealmRoles($realmRoles);
        return $this;
    }

    public function subGroups(array $subGroups): GroupCreateResourceInterface
    {
        $this->builder->withSubGroups($subGroups);
        return $this;
    }

    public function save(): GroupResourceInterface
    {
        $group = $this->builder->build();
        return $this->groupsResource->add($group);
    }
}
