<?php

namespace Scito\Keycloak\Admin\Resources;

use IteratorAggregate;
use Scito\Keycloak\Admin\Representations\GroupRepresentationInterface;
use Scito\Keycloak\Admin\Representations\RepresentationCollectionInterface;

interface GroupSearchResourceInterface extends IteratorAggregate
{
    public function offset(int $offset): self;

    public function limit(int $limit): self;


    public function id(string $email): self;

    public function name(string $name): self;

    public function path(string $path): self;



    public function query(string $query): self;

    /**
     * @return RepresentationCollectionInterface
     */
    public function get();

    /**
     * @return GroupRepresentationInterface|null
     */
    public function first();
}
