<?php

namespace Scito\Keycloak\Admin\Resources;

use Scito\Keycloak\Admin\Representations\GroupRepresentationInterface;


interface UserGroupResourceInterface
{
    public function toRepresentation(): GroupRepresentationInterface;

//    public function access(): AccessResourceInterface;

//    public function attributes(): AttributesResourceInterface;

//    public function clientRoles(): ClientRolesResourceInterface;

    public function getId(): string;

    public function getName(): string;

    public function getPath(): string;

    /**
     * @return string[]
     */
    public function getRealmRoles(): iterable;

    /**
     * @return GroupsRepresentationInterface
     */
    public function getSubGroups(): GroupsRepresentationInterface;

// actions
    public function delete(): void;
}
