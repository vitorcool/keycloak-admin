<?php

namespace Scito\Keycloak\Admin\Resources;


use Scito\Keycloak\Admin\Representations\GroupRepresentationInterface;
use Scito\Keycloak\Admin\Representations\GroupsRepresentationInterface;
use Scito\Keycloak\Admin\Representations\RepresentationCollectionInterface;

interface GroupsResourceInterface
{
    /**
     * @param string $id
     * @return GroupResourceInterface
     */
    public function get(string $id): GroupResourceInterface;
    public function getByName(string $name): GroupResourceInterface;

    /**
     * @return RepresentationCollectionInterface
     */
    public function All(): RepresentationCollectionInterface;

    public function count(): int;

    public function add(GroupRepresentationInterface $group): GroupResourceInterface;

    public function create(?array $options = null): GroupCreateResourceInterface;


}
