<?php

namespace Scito\Keycloak\Admin\Resources;

use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use Scito\Keycloak\Admin\Exceptions\CannotAddUserGroupException;
use Scito\Keycloak\Admin\Exceptions\CannotDeleteUserGroupException;
use Scito\Keycloak\Admin\Exceptions\CannotRetrieveUserGroupByNameException;
use Scito\Keycloak\Admin\Hydrator\HydratorInterface;
use Scito\Keycloak\Admin\Representations\GroupRepresentationInterface;
use Scito\Keycloak\Admin\Representations\RepresentationCollectionInterface;
use Scito\Keycloak\Admin\Representations\RepresentationCollection;
class UserGroupsResource implements UserGroupsResourceInterface
{
    /**
     * @var Client
     */
    private $client;
    /**
     * @var string
     */
    private $realm;

    private $resourceFactory;

    private $hydrator;

    private $userId=null;

    public function __construct(
        ClientInterface $client,
        ResourceFactoryInterface $resourceFactory,
        HydratorInterface $hydrator,
        string $realm,
        string $userId=null
    ) {
        $this->client = $client;
        $this->realm = $realm;
        $this->hydrator = $hydrator;
        $this->resourceFactory = $resourceFactory;
        $this->userId = $userId;
    }

    private function resUrl()
    {
        return "/auth/admin/realms/{$this->realm}/users/{$this->userId}/groups";
    }

    public function count(): int
    {
        $response = $this->client->post($this->resUrl()."/count");
        return $response;
    }

    public function add(GroupRepresentationInterface $group): bool
    {

        $url=$this->resUrl()."/{$group->getId()}";
        $response = $this->client->put($url);

        if (204 !== $response->getStatusCode()) {
            $body=(string)$response->getBody();
            $body=empty($body)?$response->getReasonPhrase():$body;
            throw new CannotAddUserGroupException("Unable to add group {$group->getId()} to user $this->userId. {$body}");
        }

//        $location = $response->getHeaderLine('Location');
//        $parts = array_filter(explode('/', $location), 'strlen');
//        $id = end($parts);
        return true;
    }

    public function get(string $id): GroupResourceInterface //UserGroupResourceInterface
    {
        $ret= $this->resourceFactory->createGroupResource($this->realm, $id);
        return $ret;
    }

    private $idOfName=[];  // need load before use
    public function getByName(string $name): UserGroupResourceInterface
    {
        // load idOfName Array Before
        $this->idOfName  = empty($this->idOfName)
            ? $this->all()->toArray()
            : $this->idOfName;

        if(is_numeric(key($this->idOfName)))
            $this->idOfName=array_combine(array_column($this->idOfName,'name'),$this->idOfName);

        $id=$this->idOfName[$name]['id'] ?? null;
        if(is_null($id)){
            throw new CannotRetrieveUserGroupByNameException("Unable to retrieve group with name {$name} ");
        }
        return $this->get($id);
    }

    /**
     * @return RepresentationCollection
     */
    public function All(): RepresentationCollectionInterface
    {
        $ret = $this->search([]);
        $ret =  $ret->get();
        return $ret;
    }

    public function search(?array $options = null): GroupSearchResourceInterface
    {
        $searchResource = $this->resourceFactory->createGroupSearchResource($this->realm,$this->userId);

        if (null !== $options) {
            foreach ($options as $k => $v) {
                $searchResource->$k($v);
            }
        }

        return $searchResource;
    }

    public function deleteById($id)
    {
        $response = $this->client->delete($this->resUrl()."/{$id}");

        if (204 != $response->getStatusCode()) {
            throw new CannotDeleteUserGroupException("User Group with id [$id] cannot be deleted");
        }

    }

}
