<?php

namespace Scito\Keycloak\Admin;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\HandlerStack;
use Scito\Keycloak\Admin\Guzzle\DefaultHeadersMiddleware;
use Scito\Keycloak\Admin\Guzzle\TokenMiddleware;
use Scito\Keycloak\Admin\Hydrator\Hydrator;
use Scito\Keycloak\Admin\Resources\ResourceFactory;
use Scito\Keycloak\Admin\Token\TokenManager;

class ClientBuilder
{
    private $guzzleConfig=[];

    private $realm;

    private $serverUrl;

    private $clientId;

    private $clientSecret;

    private $token;
    private $token_expires_in;

    private $username;

    private $password;

    private $tokenManager;

    public function __construct($attrs=[])
    {
        foreach($attrs as $attrName => $attrValue){
            if(is_null($this->$attrName)) {
                $this->$attrName = $attrValue;
            }
        }
    }

    /**
     * @param string $realm
     * @return ClientBuilder
     */
    public function withRealm(string $realm): self
    {
        $this->realm = $realm;
        return $this;
    }

    /**
     * @param string $url
     * @return ClientBuilder
     */
    public function withServerUrl(string $url): self
    {
        $this->serverUrl = $url;
        return $this;
    }

    /**
     * @param null|string $clientId
     * @return ClientBuilder
     */
    public function withClientId(?string $clientId): self
    {
        $this->clientId = $clientId;
        return $this;
    }

    /**
     * @param null|string $secret
     * @return ClientBuilder
     */
    public function withClientSecret(?string $secret): self
    {
        $this->clientSecret = $secret;
        return $this;
    }

    /**
     * @param string $username
     * @return ClientBuilder
     */
    public function withUsername(string $username): self
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @param string $password
     * @return ClientBuilder
     */
    public function withPassword(string $password): self
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @param null|string $token
     * @param null|int $token_expires_in
     * @return ClientBuilder
     */
    public function withAuthToken(?string $token,?int $token_expires_in=null): self
    {
        $this->token = $token;
        $this->token_expires_in = is_null($token_expires_in) ? time()+60 : $token_expires_in;
        return $this;
    }

    /**
     * @param array $guzzleConfig
     * @return ClientBuilder
     */
    public function withGuzzleConfig(array $guzzleConfig): self
    {
        $this->guzzleConfig = $guzzleConfig;
        return $this;
    }

    public function build()
    {
        $guzzle = $this->buildGuzzle();
        $stack = HandlerStack::create();


        if (null == ($tokenManager = $this->tokenManager)) {
            $tokenManager = $this->buildTokenManager($guzzle);
        }

        $tokenMiddleware = new TokenMiddleware($tokenManager, $this->realm);
        $stack->push($tokenMiddleware);

        $stack->push(new DefaultHeadersMiddleware());

        $client = new GuzzleClient(array_merge($this->guzzleConfig,
            ['http_errors' => false, 'handler' => $stack, 'base_uri' => $this->serverUrl]
        ));

        $factory = new ResourceFactory($client, new Hydrator());

        return new Client($factory, $this->realm, $this->clientId);
    }

    /**
     * @return ClientInterface
     */
    private function buildGuzzle()
    {
        return new GuzzleClient(array_merge($this->guzzleConfig,
            ['base_uri' => $this->serverUrl, 'http_errors' => false]
        ));
    }

    private function buildTokenManager(ClientInterface $guzzle)
    {
        if(isset($this->token)){
            return (new TokenManager($guzzle))
                ->addToken($this->realm,"Bearer",$this->token,$this->token_expires_in);
        }
        return new TokenManager($guzzle,[
            "username"=>$this->username,
            "password"=>$this->password,
            "clientId"=>$this->clientId,
            "clientSecret"=>$this->clientSecret
        ]);
    }
}
